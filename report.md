# ME 575 - Project 1: Design Project

*Due Feb 19 at 10:50 a.m.* 

**Report by Mark Redd**

For your first project, you should select a design problem from you own 
discipline, develop the model and optimize it. For mechanical engineering 
students, some possible projects might be,

- Optimizing a mechanism
- Optimizing a control system/dynamic system
- Optimizing a truss
- Optimizing a heat exchanger
- Optimizing a gear train
- Optimizing a problem from your research

Students in other disciplines should select a problem from their discipline they
are interested in. Although the selection of the model is up to you, 
there are some requirements:

- You must be involved in developing the model; you can’t just copy it from      
    somewhere. However, this doesn’t mean you have to build it from scratch.
- The model needs to include a minimum of four design variables and three design      
    functions (more is better if they are realistic).
- There should be trade-offs involved with the model so we can’t just guess the      
    solution.
- The model should be continuous and differentiable, so it can be solved by a      
    routine like fmincon. You may use whatever state-of-the-art routine or 
    language you wish.
- I would expect the scope of the project to be somewhat above the homework.

You may work on this in groups of no more than three if you wish.

Hand in hardcopy at the start of class on **<u>Feb 19</u>** with the following sections 
(please try to limit to five pages, single spaced, not including the Appendix 
or Title page):

1. **Title Page with Summary. Give a brief description of the problem** 
   **(less than 50 words) and your main results.**

2. **Procedure:**
   1. Discuss the development of the model so I can understand it. This 
      would include presenting important relationships and assumptions. Illustrations 
      are helpful. I would appreciate having some sense of the computation sequence, 
      as is given in Section 2.8.5 of the notes.
   1. Indicate how the model was tested and insured to be accurate and robust.
   1. Discuss what the optimization problem is by listing the design variables 
      and functions. 

3. **Results and Discussion of Results:** 
   1. Provide a table showing the optimum values of variables and functions, with 
      binding constraints and/or variables at bounds highlighted. How much did the 
      objective improve over the starting design? Do you feel the optimum design is
      realistic? Visual representations of the optimum are encouraged.
   1. Briefly discuss the optimum and the design space around the optimum. Include 
      contour plots as appropriate, with the feasible region shaded and the optimum 
      marked. Comment on what you learn about the design space from these. Do you 
      feel this is a global optimum? Provide support for your conclusion. 
   1. Include any other observations you feel are pertinent. These may relate to 
      the model, the results, the optimization process, the nature of the optimum, 
      etc. This section should be a half page or less.
3. **Appendix:**
   1. Listing of MATLAB or other programs

Note: Include requested items (such as graphs or tables) in their respective 
sections as given above, and not in the Appendix. Any output should be 
integrated into the report with captions, explanatory comments, etc.